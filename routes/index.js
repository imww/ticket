var express = require('express');
var router = express.Router();
const httpHelper = require('../httpHelper');

/* GET home page. */
router.get('/', async function(req, res) {
  let result = await httpHelper.getLocal('https://kyfw.12306.cn/otn/leftTicket/init');
  console.log(result)
});

module.exports = router;
